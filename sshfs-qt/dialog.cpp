#include "dialog.h"
#include "ui_dialog.h"
#include <QDebug>
#include <stdio.h>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    addressesFile = "/home/ovidiu/Desktop/tmp/sshfs-qt/addresses.txt";

//    msg.setText("lala land");
//    msg.show();

    QFile file(addressesFile);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

//    msg.setText("file opened");
//    msg.show();

    QTextStream in(&file);

    QString C_name;
    QString C_command;
    QString C_localDir;




        while (!in.atEnd()) {
            QString line = in.readLine();
            if(line.startsWith("name: "))
            {
                C_name = line.section("name: ", -1);
                qDebug() << C_name;
            }
            if(line.startsWith("server: "))
            {
                C_command = line.section("server: ", -1);
                qDebug() << C_command;
            }
            if(line.startsWith("dir: "))
            {
                C_localDir = line.section("dir: ", -1);
                qDebug() << C_localDir;
            }

            if(!C_name.isEmpty() && !C_command.isEmpty()
                    && !C_localDir.isEmpty())
            {
                connections conn;
                conn.connectionName = C_name;
                conn.command = C_command;
                conn.localDir = C_localDir;

                this->adresses.push_back(conn);

                C_name.clear();
                C_command.clear();
                C_localDir.clear();
            }
        }

        for(int i = 0; i < adresses.size(); i++)
        {
            ui->comboBox->addItem(adresses.at(i).connectionName);
        }


}

Dialog::~Dialog()
{
    delete ui;
}
