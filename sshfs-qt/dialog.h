#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QFile>
#include <QMessageBox>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    struct connections
    {
        QString connectionName;
        QString command;
        QString localDir;
    };

    QList<connections> adresses;
//    QFile *file;
    QString addressesFile;
    QMessageBox msg;

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
